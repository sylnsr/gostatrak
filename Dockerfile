FROM golang:1.8

WORKDIR /go/bin
COPY trac-amd trac
COPY static ./static
RUN chmod +x ./trac



CMD ["trac"]