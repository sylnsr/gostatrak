#!/usr/bin/env bash
# Run this script when debugging in an IDE like Intelli-J
# because in that case the app directory is /tmp and so we
# need to have a symlink from /tmp/static to point to the
# static folder in this project.
# /tmp gets cleared every reboot so run this after a reboot
cd "$(dirname "$0")"
CWD="$(pwd)"
if [ -f ./save.json ]; then
	rm ./save.json
fi
cd /tmp
ln -sf $CWD/static >> /dev/null 2>&1
ln -sf $CWD/save.json >> /dev/null 2>&1
cd $CWD