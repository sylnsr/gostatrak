#!/usr/bin/env bash
cd "$(dirname "$0")"

DEST_IP="10.0.1.15"
DEST="pi@$DEST_IP:/tmp/trac/"

echo "Stopping server"
ssh "pi@$DEST_IP" "sudo service trac stop"
sleep 5

echo "Clear remote temp folder"
ssh "pi@$DEST_IP" "rm -rf /tmp/trac"
ssh "pi@$DEST_IP" "mkdir /tmp/trac"

echo "Backup existing data"
ssh "pi@$DEST_IP" "sudo cp /var/opt/trac/save.json /tmp/trac/"

echo "Recreate existing app folder"
ssh "pi@$DEST_IP" "sudo rm -rf /var/opt/trac"
ssh "pi@$DEST_IP" "sudo mkdir -p /var/opt/trac"

echo "Upload files"
rsync -r ./static "$DEST"
rsync -r ./trac "$DEST"
ssh "pi@$DEST_IP" "sudo cp -R /tmp/trac/* /var/opt/trac/"

echo "Start server"
ssh "pi@$DEST_IP" "sudo chmod +x /var/opt/trac/trac"
ssh "pi@$DEST_IP" "sudo service trac start"

echo "Reboot" # forces browser to start with cleared cache
ssh "pi@$DEST_IP" "sudo reboot"
