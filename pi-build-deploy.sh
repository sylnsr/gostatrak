#!/usr/bin/env bash

# Use this to build and deploy to Pi
# (you must be connected to it via Wifi)

cd "$(dirname "$0")"
./build.sh pi
./playbooks/deploy.sh