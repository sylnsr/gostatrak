#!/usr/bin/env bash
cd "$(dirname "$0")"
if [[ $1 == "" ]]; then
	echo "Customer ID required as the first param"
	exit 1
fi

# clear key because this is a new host and the key wont match
ssh-keygen -f "/home/mike/.ssh/known_hosts" -R "10.0.1.11"
echo "Setting wifi on device for customer $1"

ansible-playbook access_point.yml \
-i inventory/standard.yml \
-e "cust_id=$1"