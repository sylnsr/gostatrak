#!/usr/bin/env bash
cd "$(dirname "$0")"

read -p "Are you connected to idazco_tracking wifi network? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo "Deploying app"
    # clear key because this is a new host and the key wont match
    ssh-keygen -f "/home/mike/.ssh/known_hosts" -R "10.0.0.1"
	ansible-playbook deploy.yml -i inventory/standard.yml
	echo "#################################"
	echo "# Reboot the unit from the screen"
	echo "#################################"
else
	"Exiting so that you can connect the wifi"
	exit 1
fi

