# IDAZCO Tracking

## Setting up a new Tracking unit.

 1. Connect a 32GB micro-SD card and run `./flash-images/flash.sh` to flash the Raspbian base image to the SD card.
 1. After the flash, **eject** and remove the card and place it into a Raspberry Pi "unit". 
 1. Connect a keyboard, the Edimax wifi USB, then the power.
 1. In the Welcome screen, set the country to USA and check the box for US Keyboard
 1. Set the default password to `0000000` (seven zeros)
 1. Skip connecting to the wifi network.
 1. Skip the software update and complete the "Welcome" setup.
 1. Reboot. Verify the "Welcome" setup wizard does not run again.
 1. Connect wlan0 to the same wifi your PC is on.
 1. In a term *on the unit*, run `sudo service ssh start`
 1. Run `./playbooks/access_point.sh`. This will setup the unit as an access point and restart it. **REMOVE WIFI USB** before it starts up again, so that the unit will use its own wifi hardware for the access point.
 1. Connect to the `idazco_tracking` wifi network with your PC.
 1. Run `./playbooks/deploy.sh` to deploy the app.
 1. As instructed, reboot the unit from its screen. It should now start up with the app running in full screen.
 1. Test the application to complete