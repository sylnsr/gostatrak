#!/usr/bin/env bash
cd "$(dirname "$0")"
if [ ! -f ./2018-11-13-raspbian-stretch.zip ]; then
	unzip ./2018-11-13-raspbian-stretch.zip
fi
sudo /opt/etcher/balena-etcher -c ./2018-11-13-raspbian-stretch.img
