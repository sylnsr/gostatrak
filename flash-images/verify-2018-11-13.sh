#!/usr/bin/env bash
cd "$(dirname "$0")"
FILE_NAME="./2018-11-13-raspbian-stretch.zip"
echo "Wait .."
FILE_SHA=$(sha256sum "$FILE_NAME" | awk '{print $1;}')

SHA_IS="a121652937ccde1c2583fe77d1caec407f2cd248327df2901e4716649ac9bc97"

if [[ "$FILE_SHA" == "$SHA_IS" ]]; then
	echo "Verified"
else
	echo "Verification FAILED!"
	echo "Expected:"
	echo "$SHA_IS"
	echo "Got this instead:"
	echo "$FILE_SHA"
fi