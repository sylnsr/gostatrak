package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	cogoenv "github.com/idazco/cogo/environment"
	"github.com/orcaman/concurrent-map"
	"gitlab.com/sylnsr/cogo/log"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"
)

var cfg_port = "8080"
var chosen_mac = ""
var has_mac_match = false
var channels = cmap.New()
var channels_list_json_bytes = []byte{}
var app_path = ""
var srv *http.Server

type chan_dat struct {
	Type string `json:"type"`
	Val  string `json:"val"`
}

func createMuxRoutes(r *mux.Router) {
	r.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("OK"))
	}).Methods("GET")

	r.HandleFunc("*", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}).Methods("OPTIONS")

	r.HandleFunc("/web", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/web/", http.StatusPermanentRedirect)
	}).Methods("OPTIONS", "GET")

	r.HandleFunc("/license", func(w http.ResponseWriter, r *http.Request) {
		if !has_mac_match {
			w.WriteHeader(401)
			w.Write([]byte("Unlicensed system"))
			return
		}
		w.WriteHeader(200)
		w.Write([]byte("OK"))
	}).Methods("GET")

	r.HandleFunc("/wifi", func(w http.ResponseWriter, r *http.Request) {
		m, ok := get_wifi_pass()
		w.WriteHeader(200)
		j := fmt.Sprintf("{\"message\":\"%s\",\"ok\":%t}", m, ok)
		w.Write([]byte(j))
	}).Methods("GET")

	///////////////////
	// CHANNEL ROUTES :

	// Channel: list
	r.HandleFunc("/ch/list", ChannelList).Methods("GET")

	// Channel: set
	r.HandleFunc("/ch/{name}/set", ChannelSet).Methods("POST")

	// Channel: get
	r.HandleFunc("/ch/{name}", ChannelRead).Methods("GET")

	// Channel: delete
	r.HandleFunc("/ch/{name}/delete", ChannelRemove).Methods("GET")
}

///////////

func ChannelList(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write(channels_list_json_bytes)
}

func ChannelSet(w http.ResponseWriter, r *http.Request) {
	if !has_mac_match {
		reply500(w, "Unlicensed system. Settings not saved.")
		return
	}

	name, _ := mux.Vars(r)["name"]
	if d, e := ioutil.ReadAll(r.Body); e != nil {
		reply500(w, "Can't read the body")
		return
	} else {
		c := chan_dat{}
		if e2 := json.Unmarshal(d, &c); e2 != nil {
			reply500(w, "Invalid body")
			return
		}
		channels.Set(name, c)
		replyOK(w)
	}
	save()
}

func ChannelRemove(w http.ResponseWriter, r *http.Request) {

	name, _ := mux.Vars(r)["name"]
	if _, ok := channels.Get(name); ok {
		channels.Remove(name)
	}
	replyOK(w)
	save()
}

func ChannelRead(w http.ResponseWriter, r *http.Request) {

	name, _ := mux.Vars(r)["name"]
	if data, ok := channels.Get(name); !ok {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Not found"))
		return
	} else {
		if j, err := json.Marshal(data); err != nil {
			reply500(w, "Something went wrong")
			return
		} else {
			w.WriteHeader(http.StatusOK)
			w.Write(j)
		}
	}
}

////////////
func replyMessage(w http.ResponseWriter, message string) {
	j := fmt.Sprintf("{\"message\":\"%s\"}", strings.Replace(message, "\"", "'", 0))
	w.Write([]byte(j))
}

func reply500(w http.ResponseWriter, message string) {
	w.WriteHeader(500)
	replyMessage(w, message)
}

func replyOK(w http.ResponseWriter) {
	w.WriteHeader(http.StatusOK)
	replyMessage(w, "OK")
}

////////////
func main() {

	// Store data + config in the app path
	var err error
	app_path, err = cogoenv.AppPath()
	if err != nil {
		log.Fatal("unable to set the application's working directory", err)
		os.Exit(1)
	}
	log.Info("application path is " + app_path)
	init_config()

	for name, mac := range cogoenv.MacAddresses() {
		if !strings.HasPrefix(name, "lo") {
			log.Info("Found " + strings.ToLower(mac))
			if strings.ToLower(mac) == strings.ToLower(chosen_mac) {
				has_mac_match = true
				break
			}
		}
	}
	if !has_mac_match {
		log.Error("Unlicensed system:", errors.New("no licensed interface found"))
	} else {
		log.Info("License OK")
	}

	// Web server
	r := mux.NewRouter()
	// Static files
	r.PathPrefix("/web/").Handler(http.StripPrefix("/web/", http.FileServer(http.Dir(app_path+"/static/"))))
	// App routes
	createMuxRoutes(r)
	// redirect from root
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/web/", http.StatusPermanentRedirect)
	}).Methods("GET")

	m := fmt.Sprintf("starting server on http://0.0.0.0:%s", cfg_port)
	log.Info(m)
	log_string(m)

	srv = &http.Server{
		Handler:      r,
		Addr:         "0.0.0.0:" + cfg_port,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	go func() {
		err = srv.ListenAndServe()
	}()

	// Setting up signal capturing
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	// Waiting for SIGINT (pkill -2)
	<-stop
}

func init_config() bool {
	// config from env vars
	for _, e := range os.Args {
		pair := strings.Split(e, "=")
		switch pair[0] {
		case "GOSTATRACK_PORT":
			cfg_port = pair[1]
		case "CHOSEN_MAC":
			chosen_mac = pair[1]
		}
	}
	restore()
	return true
}

func save() {
	if j, err := json.Marshal(channels); err != nil {
		m := "failed to save channels"
		log.Error(m, err)
		log_string(m)
	} else {
		channels_list_json_bytes = j
		if _, err := os.Stat(save_file_name()); !os.IsNotExist(err) {
			os.Remove(save_file_name())
		}
		writeFile(save_file_name(), j, 0644)
	}
}

func restore() {
	if _, err := os.Stat(save_file_name()); !os.IsNotExist(err) {
		if d, e := ioutil.ReadFile(save_file_name()); e != nil {
			log_string("failed to restore data from " + save_file_name())
		} else {
			tmp_data := make(map[string]chan_dat)
			if e2 := json.Unmarshal(d, &tmp_data); e2 != nil {
				m := "failed to parse data from " + save_file_name()
				log_string(m)
				log.Error(m, e2)
			} else {
				for k, v := range tmp_data {
					channels.Set(k, v)
				}
				channels_list_json_bytes = d
				m := "restored data from " + save_file_name()
				log.Info(m)
				log_string(m)
			}
		}
		return
	}
	channels_list_json_bytes = []byte("{}")
}

func save_file_name() string {
	return app_path + "/save.json"
}

func log_string(m string) {
	currentTime := time.Now()
	p := app_path + "/" + currentTime.Format("2006-01-02") + ".log"
	t := currentTime.Format("15:04:05")
	appendFile(p, []byte(t+" - "+m+"\n"), 0644)
}

// File writing
func writeFile(filename string, data []byte, perm os.FileMode) error {
	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, perm)
	if err != nil {
		return err
	}
	n, err := f.Write(data)
	if err == nil && n < len(data) {
		err = io.ErrShortWrite
	}
	if err1 := f.Close(); err == nil {
		err = err1
	}
	return err
}

func appendFile(filename string, data []byte, perm os.FileMode) error {
	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_APPEND, perm)
	if err != nil {
		return err
	}
	n, err := f.Write(data)
	if err == nil && n < len(data) {
		err = io.ErrShortWrite
	}
	if err1 := f.Close(); err == nil {
		err = err1
	}
	return err
}

func get_wifi_pass() (m string, ok bool) {
	conf_f := "/etc/hostapd/hostapd.conf"
	conf_l := "wpa_passphrase="

	// see if we have wifi config
	if _, err := os.Stat(conf_f); os.IsNotExist(err) {
		m = "This isn't a WIFI system"
		return
	}
	if d, e := ioutil.ReadFile(conf_f); e != nil {
		m = "Error reading WIFI password file"
		log_string(m)
		return
	} else {
		for _, l := range strings.Split(string(d[:]), "\n") {
			s := strings.Trim(l, " ")
			if strings.HasPrefix(s, conf_l) {
				m = strings.Split(s, "=")[1]
				m = strings.Trim(m, " ")
			}
		}
	}
	return
}
