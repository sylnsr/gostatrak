#!/usr/bin/env bash
cd "$(dirname "$0")"

# Pi
GOOS=linux GOARCH=arm go build -v .
mv ./gostatrak ./trac
# a pi only build
if [[ $1 == "pi" ]]; then
	exit 0
fi

# Docker
GOOS=linux GOARCH=amd64 go build -v .
mv ./gostatrak ./trac-amd
docker build -t idazco/tracker:latest .