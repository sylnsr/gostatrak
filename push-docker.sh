#!/usr/bin/env bash

ECR_URL="235118705782.dkr.ecr.us-west-2.amazonaws.com"

docker tag idazco/tracker:latest "$ECR_URL"/tracker
docker push "$ECR_URL"/tracker