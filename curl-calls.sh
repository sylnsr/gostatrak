#!/usr/bin/env bash

function continue() {
	echo
	read -p "Continue? (Y/n) " -n 1 -r
	if [[ ! $REPLY =~ ^[Nn]$ ]]; then
		echo
	else
		exit 1
	fi
}

echo "> list channels, there will be none"
curl -X GET http://localhost:3000/ch/list
continue

echo "> add a channel"
curl -X POST -d '{"color":"blue"}' http://localhost:3000/ch/blue-node/set
echo
echo "> list channels again, there will be one"
curl -X GET http://localhost:3000/ch/list
continue

echo "> add another channel"
curl -X POST -d '{"color":"red"}' http://localhost:3000/ch/red-node/set
echo
echo "> list channels again, there will be two"
curl -X GET http://localhost:3000/ch/list
continue

echo "> remove a channel and list, there will be one"
curl -X GET http://localhost:3000/ch/red-node/delete
echo
curl -X GET http://localhost:3000/ch/list
continue

echo "> remove last channel"
curl -X GET http://localhost:3000/ch/blue-node/delete
echo
echo "> list again, now there are none"
curl -X GET http://localhost:3000/ch/list
continue

echo "> .. done."